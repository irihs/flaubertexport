package org.irihs.omeka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class Omeka2Nakala
{
	public static void readBatchExport(File file, Map<String, Map<String, String>> handles) throws Exception
	{
		BufferedReader in = new BufferedReader(new FileReader(file));
		String line = null;
		while ((line = in.readLine()) != null)
		{
			String [] toks = line.split(",");
			String [] path = toks[0].split("/");
			Map<String, String> map = handles.get(path[0]);
			if (map == null)
				handles.put(path[0], map = new HashMap<>());
			map.put(path[path.length-1], toks[1]);
		}
		in.close();
	}
	
	public static void insertHandles(File inFile, File outFile, Map<String, Map<String, String>> handles, int padding, String sourceToFile) throws Exception
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), "UTF-8"));
		OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(outFile, false), "UTF-8");
		String line = null;
		while ((line = in.readLine()) != null)
		{
			if (line.contains("<Folio"))
			{
				int f = line.indexOf("folio=\"");
				if (f < 0) throw new Exception("Can't find folio: "+line);
				f += "folio=\"".length();
				int e = line.indexOf("\"", f);
				String folio = line.substring(f, e);
				while (folio.length() < padding)
					folio = "0"+folio;
				
				f = line.indexOf("recto_verso=\"");
				if (f < 0) throw new Exception("Can't find recto_verso: "+line);
				f += "recto_verso=\"".length();
				e = line.indexOf("\"", f);
				String rv = line.substring(f, e);
				
				f = line.indexOf("emplacement=\"");
				if (f < 0) throw new Exception("Can't find emplacement: "+line);
				f += "emplacement=\"".length();
				e = line.indexOf("\"", f);
				String pos = line.substring(f, e);
				
				f = line.indexOf("source=\"");
				if (f < 0) throw new Exception("Can't find source: "+line);
				f += "source=\"".length();
				e = line.indexOf("\"", f);
				String source = line.substring(f, e);
				if (source.contains(" "))
					source = source.split(" ")[0];
				
				String key = folio+(!rv.equals("null") ? rv : "")+(!pos.equals("null") ? pos.toLowerCase() : "")+".jpg";
				Map<String, String> map = handles.get(source);
				if (map != null)
				{
					String handle = map.get(key);
					if (handle != null)
						line = line.replace("folio=\"", "url=\"www.nakala.fr/nakala/data/"+handle+"\" folio=\"");
					else if (new File(inFile.getParent()+"/"+source+"/"+sourceToFile+key).exists()) 
						System.out.println("No handle for "+source+" "+key);
					//else System.out.println("No file for "+source+" "+key);
				}
			}
			out.write(line);
			out.write("\n");
		}
		in.close();
		out.close();
	}
	
	public static void main(String [] args) throws Exception
	{
		Map<String, Map<String, String>> handles;
		
		handles = new HashMap<>();
		readBatchExport(new File("nakala\\Plans Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 1 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 2 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 3 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 4 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 5 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 6 Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\Copiste Mme Bovary.csv"), handles);
		readBatchExport(new File("nakala\\D�finitif Mme Bovary.csv"), handles);
		insertHandles(
			new File("bovary\\bovary.xml"), 
			new File("bovary\\bovaryNakala.xml"), 
			handles, 3, "jpg/");
		
		handles = new HashMap<>();
		readBatchExport(new File("nakala\\Brouillons 226 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 227 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Plans Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 1 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 2 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 3 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 4 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 5 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 6 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 7 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 8 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 9 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\Brouillons 10 Bouvard et P�cuchet.csv"), handles);
		readBatchExport(new File("nakala\\D�finitif Bouvard et P�cuchet.csv"), handles);
		insertHandles(
			new File("bouvardPecuchet\\bouvardPecuchet.xml"), 
			new File("bouvardPecuchet\\bouvardPecuchetNakala.xml"), 
			handles, 4, "");
	}
}
