package org.irihs.omeka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import com.mysql.jdbc.Driver;

@SuppressWarnings("deprecation")
public class Flaubert2Xml
{
	public static void extract(String url, String user, String password, String corpus, String footer, String help, File outFile) throws Exception
	{
		File censoredFile = new File(outFile.getParentFile(), "censure.txt");
		Map<String, String> censored = new HashMap<>();
		if (censoredFile.exists())
		{
			String data = FileUtils.readFileToString(censoredFile, "UTF-8");
			int index = 0;
			while (true)
			{
				int next = data.indexOf("<Folio>", index);
				if (next < 0)
					break;
				int end = data.indexOf("</Folio>", index);
				String folio = data.substring(next+7, end);
				next = data.indexOf("<Passage>", index);
				end = data.indexOf("</Passage>", index);
				censored.put(folio, data.substring(next+9, end));
				index = end+10;
			}
		}
		
		OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8");
		out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<Flaubert>\n");
		
		Driver driver = (Driver)Class.forName("com.mysql.jdbc.Driver").newInstance();
		Properties props = new Properties();
		props.setProperty("user", user);
		props.setProperty("password", password);
		Connection con = driver.connect(url, props);
		
		out.write("\t<Help>\n");
		out.write(help);
		out.write("\t</Help>\n");
		
		out.write("\t<Footer>\n");
		out.write(footer);
		out.write("\t</Footer>\n");
		
		out.write("\t<Table name=\"Plans\" etats=\"");
		ResultSet rs = con.createStatement().executeQuery("SELECT DISTINCT(fr.etat) as etat FROM fragments fr WHERE fr.corpus='"+corpus+"' AND fr.groupe='P'");
		boolean first = true;
		while (rs.next())
			{out.write((first ? "" : " ")+rs.getString("etat")+"P"); first = false;}
		out.write("\" />\n");
		out.write("\t<Table name=\"Brouillons\" etats=\"");
		rs = con.createStatement().executeQuery("SELECT DISTINCT(fr.etat) as etat FROM fragments fr WHERE fr.corpus='"+corpus+"' AND fr.groupe='B'");
		first = true;
		while (rs.next())
			{out.write((first ? "" : " ")+rs.getString("etat")+"B"); first = false;}
		out.write("\" />\n");
		
		rs = con.createStatement().executeQuery("SELECT DISTINCT(fr.source) as src FROM fragments fr WHERE fr.corpus='"+corpus+"'");
		while (rs.next())
			out.write("\t<Source name=\""+rs.getString("src")+"\" />\n");
		rs.close();
		
		rs = con.createStatement().executeQuery("SELECT f.id as id, f.folio as folio, f.recto_verso as recto_verso, f.emplacement as emplacement, f.source as src, f.transcription as transcription, f.notes_regie as notes "
				+ "FROM folios f WHERE f.corpus='"+corpus+"'");
		while (rs.next())
		{
			out.write("\t<Folio id=\""+rs.getString("id")+"\" "
				+ "folio=\""+rs.getString("folio")+"\" "
				+ "recto_verso=\""+rs.getString("recto_verso")+"\" "
				+ "emplacement=\""+rs.getString("emplacement")+"\" "
				+ "notes=\""+StringEscapeUtils.escapeXml(rs.getString("notes"))+"\" "
				+ "censure=\""+StringEscapeUtils.escapeXml(censored.get(rs.getString("src")+rs.getString("folio")))+"\" "
				+ "source=\""+rs.getString("src")+"\" ");
			if (rs.getString("src").equals("F"))
			{
				ResultSet rs2 = con.createStatement().executeQuery("SELECT * FROM en_tetes WHERE page="+rs.getString("folio")+" AND corpus='"+corpus+"'");
				while (rs2.next())
				{
					out.write("incipit=\""+StringEscapeUtils.escapeXml(rs2.getString("debut"))+"\" explicit=\""+StringEscapeUtils.escapeXml(rs2.getString("fin"))+"\"");
					break;
				}
				rs2.close();
			}
			out.write(">\n");
			String transcription = rs.getString("transcription");
			if (transcription != null)
				out.write(transcription.replace("\"", "&quot;").replace("<", "&lt;").replace("&", "&amp;"));
			out.write("\n\t</Folio>\n");
		}
		rs.close();
		
		rs = con.createStatement().executeQuery(
				"SELECT id, id_folio, etat, emplacement, debut_page, debut_ligne, fin_page, fin_ligne, f_origine, f_prec_vertical, present, groupe "
				+ "FROM fragments "
				+ "WHERE corpus='"+corpus+"' AND (groupe='B' OR groupe='P')");
		while (rs.next())
		{
			int fromPage = Integer.parseInt(rs.getString("debut_page"));
			int toPage = Integer.parseInt(rs.getString("fin_page"));
			out.write("\t<Fragment id=\""+rs.getString("id")+"\" "
				+ "id_folio=\""+rs.getString("id_folio")+"\" "
				+ "etat=\""+rs.getString("etat")+rs.getString("groupe")+"\" "
				+ "emplacement=\""+rs.getString("emplacement")+"\" "
				+ "debut_page=\""+fromPage+"\" "
				+ "debut_ligne=\""+rs.getString("debut_ligne")+"\" "
				+ "fin_page=\""+toPage+"\" "
				+ "fin_ligne=\""+rs.getString("fin_ligne")+"\" "
				+ "f_origine=\""+rs.getString("f_origine")+"\" "
				+ "f_prec_vertical=\""+rs.getString("f_prec_vertical")+"\" "
				+ "present=\""+rs.getString("present")+"\" "
				+ "/>\n");
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(outFile.getParentFile(), "chapters.xml")), "UTF-8"));
		String line = null;
		while ((line = in.readLine()) != null)
			out.write("\t"+line+"\n");
		in.close();
		
		out.write("</Flaubert>\n");
		out.close();
		con.close();
	}
	
	static String bpHelp = "<table border=\"0\" cellpadding=\"20\" cellspacing=\"0\" width=\"100%\"><tr><td width=\"100%\" height=\"40\"><div align=\"left\"><center>CODES DE TRANSCRIPTION<br/><br/></center><ul><li>Le texte principal qui correspond au premier jet de Flaubert est en caract&egrave;res romains et en noir.</li><li>Les ajouts interlin&eacute;aires sont en <span class=\"Style2\">italiques bleus</span></i>.</li><li>Ce qui est barr&eacute; est <strike>barr&eacute;</strike>.</li><li>Ce qui est soulign&eacute; est <u>soulign&eacute;</u>.</li><li>Les passages biff&eacute;s sont encadr&eacute;s par une double accolade {{&nbsp;&nbsp;&nbsp;}} et apparaissent &agrave; l'&eacute;cran sur fond jaune.</li><li>Les passages barr&eacute;s d'une croix de Saint-Andr&eacute; sont encadr&eacute;s par un double crochet [[&nbsp;&nbsp;&nbsp;]].</li><li>Les mots incertains sont suivis d'un ast&eacute;risque. Deux ast&eacute;risques encadrent une suite de mots incertains.</li><li>Les mots ou les passages illisibles sont marqu&eacute;s [illis.]. On signale par des points de part et d'autre la longueur du texte non d&eacute;chiffr&eacute;.</li><li>Nous avons choisi de restituer les accents.</li><li>En revanche les fautes, les abr&eacute;viations, les graphies anciennes sont maintenues.</li></ul></div></td></tr></table>";
	static String bpFooter = "Le tableau pr�sente les folios depuis le premier �tat (en bas) jusqu'au texte publi� (en haut).<br>Classement g�n�tique : Yvan Leclerc et Jo�lle Robert � Coordination et relectures : Danielle Girard";
	static String bovHelp = "<table border=\"1\" cellpadding=\"20\" cellspacing=\"0\" width=\"100%\" bordercolor=\"#940000\"><tr><td width=\"100%\" height=\"40\"><div align=\"left\"><center>Codes des transcriptions&nbsp;: </center><ul><li>Le texte principal qui correspond au premier jet de Flaubert ou &agrave; la mise au propre du brouillon pr&eacute;c&eacute;dent est en caract&egrave;res romains et en noir.</li><li>Les ajouts marginaux ou interlin&eacute;aires sont en <span class=\"Style2\">italiques bleus</span></i>.</li><li>Ce qui est barr&eacute; est <strike>barr&eacute;</strike>.</li><li>Ce qui est soulign&eacute; est <u>soulign&eacute;</u>.</li><li>Les passages biff&eacute;s sont encadr&eacute;s par une double accolade {{&nbsp;&nbsp;&nbsp;}} et apparaissent &agrave; l'&eacute;cran sur fond jaune.</li><li>Les passages barr&eacute;s d'une croix de Saint-Andr&eacute; sont encadr&eacute;s par un double crochet [[&nbsp;&nbsp;&nbsp;]].</li><li>Les mots incertains sont suivis d'un ast&eacute;risque. Deux ast&eacute;risques encadrent une suite de mots incertains.</li><li>Les mots ou les passages illisibles sont marqu&eacute;s [illis.]. On signale par des points de part et d'autre la longueur du texte non d&eacute;chiffr&eacute;.</li><li>Nous avons choisi de restituer les accents.</li><li>En revanche les fautes, les abr&eacute;viations, les graphies anciennes sont maintenues.</li></ul></div></td></tr></table>";
	static String bovFooter = "Le tableau pr�sente les folios depuis le premier �tat (en bas) jusqu'au texte publi� (en haut).<br>Classement g�n�tique : Marie Durel - Coordination des transcriptions : Danielle Girard";
	
	public static void main(String [] args) throws Exception
	{
		extract(
			"jdbc:mysql://localhost/flaubert", 
			"root", 
			"", 
			"b", 
			StringEscapeUtils.escapeXml(bovFooter),
			StringEscapeUtils.escapeXml(bovHelp),
			new File("bovary\\bovary.xml"));
		extract(
			"jdbc:mysql://localhost/flaubert", 
			"root", 
			"", 
			"p", 
			StringEscapeUtils.escapeXml(bpFooter), 
			StringEscapeUtils.escapeXml(bpHelp),
			new File("bouvardPecuchet\\bouvardPecuchet.xml"));
		
//		rename();
	}
}
